﻿using Hoteles.Models.Dto;

namespace Hoteles.Intefaces.RepositoryPattern
{
    public interface ILoginRepository
    {
        //public Task<bool> ValidarLogin(string email, string password);
        public string LoginByToken(string loginToken);
        public string obtenerTokenLogin(string email, string password);
        public bool ValidarTokenUsuario(string tokenUsuario);
    }


}
