﻿using Hoteles.Models.Dto;

namespace Hoteles.Intefaces.RepositoryPattern
{
    public interface IReservaRepository
    {
        public Task<List<ReservaInfoDto>> ObtenerReservas();
        public Task<List<ReservaDisponibleDto>> ObtenerOpcionesReservas(ReservaFiltrosDto filtros);
    }
}
