﻿using Hoteles.Models;
using Hoteles.Models.Dto;

    public interface IMailService
    {
        bool SendMail(MailData mailData);
    }

