﻿using System;
using System.Collections.Generic;

namespace Hoteles.Models.Entities
{
    public partial class TipoHabitacion
    {
        public TipoHabitacion()
        {
            Habitacions = new HashSet<Habitacion>();
        }

        public int Id { get; set; }
        public string? Descripcion { get; set; }
        public int? FkEstadoId { get; set; }

        public virtual Estado? FkEstado { get; set; }
        public virtual ICollection<Habitacion> Habitacions { get; set; }
    }
}
