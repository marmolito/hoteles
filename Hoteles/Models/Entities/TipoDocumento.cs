﻿using System;
using System.Collections.Generic;

namespace Hoteles.Models.Entities
{
    public partial class TipoDocumento
    {
        public TipoDocumento()
        {
            Personas = new HashSet<Persona>();
        }

        public int Id { get; set; }
        public string? Documento { get; set; }
        public int? FkEstadoId { get; set; }

        public virtual Estado? FkEstado { get; set; }
        public virtual ICollection<Persona> Personas { get; set; }
    }
}
