﻿using System;
using System.Collections.Generic;

namespace Hoteles.Models.Entities
{
    public partial class Ciudad
    {
        public Ciudad()
        {
            Hotels = new HashSet<Hotel>();
        }

        public int Id { get; set; }
        public string? Nombre { get; set; }
        public int? FkDepartamento { get; set; }
        public int? FkEstadoId { get; set; }

        public virtual Departamento? FkDepartamentoNavigation { get; set; }
        public virtual Estado? FkEstado { get; set; }
        public virtual ICollection<Hotel> Hotels { get; set; }
    }
}
