﻿using System;
using System.Collections.Generic;

namespace Hoteles.Models.Entities
{
    public partial class Habitacion
    {
        public Habitacion()
        {
            Reservas = new HashSet<Reserva>();
        }

        public int Id { get; set; }
        public string? Numero { get; set; }
        public double? CostoBase { get; set; }
        public int? Impuestos { get; set; }
        public int? CantidadAlojamiento { get; set; }
        public string? Descripcion { get; set; }
        public bool? Desayuno { get; set; }
        public bool? CancelacionGratuita { get; set; }
        public int? FkTipoId { get; set; }
        public int? FkCamasId { get; set; }
        public int? FkHotelId { get; set; }
        public int? FkEstadoId { get; set; }

        public virtual Cama? FkCamas { get; set; }
        public virtual Estado? FkEstado { get; set; }
        public virtual Hotel? FkHotel { get; set; }
        public virtual TipoHabitacion? FkTipo { get; set; }
        public virtual ICollection<Reserva> Reservas { get; set; }
    }
}
