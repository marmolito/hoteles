﻿using System;
using System.Collections.Generic;

namespace Hoteles.Models.Entities
{
    public partial class Genero
    {
        public Genero()
        {
            Personas = new HashSet<Persona>();
        }

        public int Id { get; set; }
        public string? Descripcion { get; set; }
        public int? FkEstadoId { get; set; }

        public virtual Estado? FkEstado { get; set; }
        public virtual ICollection<Persona> Personas { get; set; }
    }
}
