﻿using System;
using System.Collections.Generic;
using Hoteles.Models.Dto;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Hoteles.Models.Entities
{
    public partial class HotelesContext : DbContext
    {
        public HotelesContext()
        {
        }

        public HotelesContext(DbContextOptions<HotelesContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Cama> Camas { get; set; } = null!;
        public virtual DbSet<Ciudad> Ciudads { get; set; } = null!;
        public virtual DbSet<Departamento> Departamentos { get; set; } = null!;
        public virtual DbSet<Estado> Estados { get; set; } = null!;
        public virtual DbSet<Extension> Extensions { get; set; } = null!;
        public virtual DbSet<Genero> Generos { get; set; } = null!;
        public virtual DbSet<Habitacion> Habitacions { get; set; } = null!;
        public virtual DbSet<Hotel> Hotels { get; set; } = null!;
        public virtual DbSet<Pais> Pais { get; set; } = null!;
        public virtual DbSet<Persona> Personas { get; set; } = null!;
        public virtual DbSet<Reserva> Reservas { get; set; } = null!;
        public virtual DbSet<TipoDocumento> TipoDocumentos { get; set; } = null!;
        public virtual DbSet<TipoHabitacion> TipoHabitacions { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Cama>(entity =>
            {
                entity.ToTable("cama");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("descripcion");

                entity.Property(e => e.FkEstadoId).HasColumnName("fk_estado_id");

                entity.HasOne(d => d.FkEstado)
                    .WithMany(p => p.Camas)
                    .HasForeignKey(d => d.FkEstadoId)
                    .HasConstraintName("FK__cama__fk_estado___49C3F6B7");
            });

            modelBuilder.Entity<Ciudad>(entity =>
            {
                entity.ToTable("ciudad");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.FkDepartamento).HasColumnName("fk_departamento");

                entity.Property(e => e.FkEstadoId).HasColumnName("fk_estado_id");

                entity.Property(e => e.Nombre)
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("nombre");

                entity.HasOne(d => d.FkDepartamentoNavigation)
                    .WithMany(p => p.Ciudads)
                    .HasForeignKey(d => d.FkDepartamento)
                    .HasConstraintName("FK__ciudad__fk_depar__412EB0B6");

                entity.HasOne(d => d.FkEstado)
                    .WithMany(p => p.Ciudads)
                    .HasForeignKey(d => d.FkEstadoId)
                    .HasConstraintName("FK__ciudad__fk_estad__403A8C7D");
            });

            modelBuilder.Entity<Departamento>(entity =>
            {
                entity.ToTable("departamento");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.FkEstadoId).HasColumnName("fk_estado_id");

                entity.Property(e => e.FkPais).HasColumnName("fk_pais");

                entity.Property(e => e.Nombre)
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("nombre");

                entity.HasOne(d => d.FkEstado)
                    .WithMany(p => p.Departamentos)
                    .HasForeignKey(d => d.FkEstadoId)
                    .HasConstraintName("FK__departame__fk_es__3C69FB99");

                entity.HasOne(d => d.FkPaisNavigation)
                    .WithMany(p => p.Departamentos)
                    .HasForeignKey(d => d.FkPais)
                    .HasConstraintName("FK__departame__fk_pa__3D5E1FD2");
            });

            modelBuilder.Entity<Estado>(entity =>
            {
                entity.ToTable("estados");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Estado1).HasColumnName("estado");

                entity.Property(e => e.Nombre)
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("nombre");
            });

            modelBuilder.Entity<Extension>(entity =>
            {
                entity.ToTable("extension");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.FkEstadoId).HasColumnName("fk_estado_id");

                entity.Property(e => e.FkPaisId).HasColumnName("fk_pais_id");

                entity.Property(e => e.Numero)
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .HasColumnName("numero");

                entity.HasOne(d => d.FkEstado)
                    .WithMany(p => p.Extensions)
                    .HasForeignKey(d => d.FkEstadoId)
                    .HasConstraintName("FK__extension__fk_pa__4F7CD00D");

                entity.HasOne(d => d.FkPais)
                    .WithMany(p => p.Extensions)
                    .HasForeignKey(d => d.FkPaisId)
                    .HasConstraintName("FK__extension__fk_pa__5070F446");
            });

            modelBuilder.Entity<Genero>(entity =>
            {
                entity.ToTable("genero");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("descripcion");

                entity.Property(e => e.FkEstadoId).HasColumnName("fk_estado_id");

                entity.HasOne(d => d.FkEstado)
                    .WithMany(p => p.Generos)
                    .HasForeignKey(d => d.FkEstadoId)
                    .HasConstraintName("FK__genero__fk_estad__440B1D61");
            });

            modelBuilder.Entity<Habitacion>(entity =>
            {
                entity.ToTable("habitacion");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CancelacionGratuita).HasColumnName("cancelacion_gratuita");

                entity.Property(e => e.CantidadAlojamiento).HasColumnName("cantidad_alojamiento");

                entity.Property(e => e.CostoBase).HasColumnName("costo_base");

                entity.Property(e => e.Desayuno).HasColumnName("desayuno");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(1000)
                    .IsUnicode(false)
                    .HasColumnName("descripcion");

                entity.Property(e => e.FkCamasId).HasColumnName("fk_camas_id");

                entity.Property(e => e.FkEstadoId).HasColumnName("fk_estado_id");

                entity.Property(e => e.FkHotelId).HasColumnName("fk_hotel_id");

                entity.Property(e => e.FkTipoId).HasColumnName("fk_tipo_id");

                entity.Property(e => e.Impuestos).HasColumnName("impuestos");

                entity.Property(e => e.Numero)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .HasColumnName("numero");

                entity.HasOne(d => d.FkCamas)
                    .WithMany(p => p.Habitacions)
                    .HasForeignKey(d => d.FkCamasId)
                    .HasConstraintName("FK__habitacio__fk_ca__5AEE82B9");

                entity.HasOne(d => d.FkEstado)
                    .WithMany(p => p.Habitacions)
                    .HasForeignKey(d => d.FkEstadoId)
                    .HasConstraintName("FK__habitacio__fk_es__59063A47");

                entity.HasOne(d => d.FkHotel)
                    .WithMany(p => p.Habitacions)
                    .HasForeignKey(d => d.FkHotelId)
                    .HasConstraintName("FK__habitacio__fk_ho__59FA5E80");

                entity.HasOne(d => d.FkTipo)
                    .WithMany(p => p.Habitacions)
                    .HasForeignKey(d => d.FkTipoId)
                    .HasConstraintName("FK__habitacio__fk_ti__5BE2A6F2");
            });

            modelBuilder.Entity<Hotel>(entity =>
            {
                entity.ToTable("hotel");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Direccion)
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("direccion");

                entity.Property(e => e.FkCiudadId).HasColumnName("fk_ciudad_id");

                entity.Property(e => e.FkDepartamentoId).HasColumnName("fk_departamento_id");

                entity.Property(e => e.FkEstadoId).HasColumnName("fk_estado_id");

                entity.Property(e => e.FkPaisId).HasColumnName("fk_pais_id");

                entity.Property(e => e.Nit)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("nit");

                entity.Property(e => e.Nombre)
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("nombre");

                entity.HasOne(d => d.FkCiudad)
                    .WithMany(p => p.Hotels)
                    .HasForeignKey(d => d.FkCiudadId)
                    .HasConstraintName("FK__hotel__fk_ciudad__5441852A");

                entity.HasOne(d => d.FkDepartamento)
                    .WithMany(p => p.Hotels)
                    .HasForeignKey(d => d.FkDepartamentoId)
                    .HasConstraintName("FK__hotel__fk_depart__5535A963");

                entity.HasOne(d => d.FkEstado)
                    .WithMany(p => p.Hotels)
                    .HasForeignKey(d => d.FkEstadoId)
                    .HasConstraintName("FK__hotel__fk_estado__534D60F1");

                entity.HasOne(d => d.FkPais)
                    .WithMany(p => p.Hotels)
                    .HasForeignKey(d => d.FkPaisId)
                    .HasConstraintName("FK__hotel__fk_pais_i__5629CD9C");
            });

            modelBuilder.Entity<Pais>(entity =>
            {
                entity.ToTable("pais");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.FkEstadoId).HasColumnName("fk_estado_id");

                entity.Property(e => e.Nombre)
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("nombre");

                entity.HasOne(d => d.FkEstado)
                    .WithMany(p => p.Pais)
                    .HasForeignKey(d => d.FkEstadoId)
                    .HasConstraintName("FK__pais__fk_estado___398D8EEE");
            });

            modelBuilder.Entity<Persona>(entity =>
            {
                entity.ToTable("persona");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Apellidos)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("apellidos");

                entity.Property(e => e.Contraseña)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("contraseña");

                entity.Property(e => e.Correo)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("correo");

                entity.Property(e => e.FechaNacimiento)
                    .HasColumnType("date")
                    .HasColumnName("fecha_nacimiento");

                entity.Property(e => e.FkDocumentoId).HasColumnName("fk_documento_id");

                entity.Property(e => e.FkEstadoId).HasColumnName("fk_estado_id");

                entity.Property(e => e.FkExtensionContactoId).HasColumnName("fk_extension_contacto_id");

                entity.Property(e => e.FkExtensionId).HasColumnName("fk_extension_id");

                entity.Property(e => e.FkGeneroId).HasColumnName("fk_genero_id");

                entity.Property(e => e.NombreContacto)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("nombre_contacto");

                entity.Property(e => e.Nombres)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("nombres");

                entity.Property(e => e.NumeroDocumento)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("numero_documento");

                entity.Property(e => e.Telefono)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("telefono");

                entity.Property(e => e.TelefonoContacto)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("telefono_contacto");

                entity.HasOne(d => d.FkDocumento)
                    .WithMany(p => p.Personas)
                    .HasForeignKey(d => d.FkDocumentoId)
                    .HasConstraintName("FK__persona__fk_docu__60A75C0F");

                entity.HasOne(d => d.FkEstado)
                    .WithMany(p => p.Personas)
                    .HasForeignKey(d => d.FkEstadoId)
                    .HasConstraintName("FK__persona__fk_esta__5EBF139D");

                entity.HasOne(d => d.FkExtensionContacto)
                    .WithMany(p => p.PersonaFkExtensionContactos)
                    .HasForeignKey(d => d.FkExtensionContactoId)
                    .HasConstraintName("FK__persona__fk_exte__628FA481");

                entity.HasOne(d => d.FkExtension)
                    .WithMany(p => p.PersonaFkExtensions)
                    .HasForeignKey(d => d.FkExtensionId)
                    .HasConstraintName("FK__persona__fk_exte__619B8048");

                entity.HasOne(d => d.FkGenero)
                    .WithMany(p => p.Personas)
                    .HasForeignKey(d => d.FkGeneroId)
                    .HasConstraintName("FK__persona__fk_gene__5FB337D6");
            });

            modelBuilder.Entity<Reserva>(entity =>
            {
                entity.ToTable("reserva");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.FechaFin)
                    .HasColumnType("date")
                    .HasColumnName("fecha_fin");

                entity.Property(e => e.FechaInicio)
                    .HasColumnType("date")
                    .HasColumnName("fecha_inicio");

                entity.Property(e => e.FkEstadoId).HasColumnName("fk_estado_id");

                entity.Property(e => e.FkHabitacion).HasColumnName("fk_habitacion");

                entity.Property(e => e.FkPersonaId).HasColumnName("fk_persona_id");

                entity.HasOne(d => d.FkEstado)
                    .WithMany(p => p.Reservas)
                    .HasForeignKey(d => d.FkEstadoId)
                    .HasConstraintName("FK__reserva__fk_esta__656C112C");

                entity.HasOne(d => d.FkHabitacionNavigation)
                    .WithMany(p => p.Reservas)
                    .HasForeignKey(d => d.FkHabitacion)
                    .HasConstraintName("FK__reserva__fk_habi__6754599E");

                entity.HasOne(d => d.FkPersona)
                    .WithMany(p => p.Reservas)
                    .HasForeignKey(d => d.FkPersonaId)
                    .HasConstraintName("FK__reserva__fk_pers__66603565");
            });

            modelBuilder.Entity<TipoDocumento>(entity =>
            {
                entity.ToTable("tipo_documento");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Documento)
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("documento");

                entity.Property(e => e.FkEstadoId).HasColumnName("fk_estado_id");

                entity.HasOne(d => d.FkEstado)
                    .WithMany(p => p.TipoDocumentos)
                    .HasForeignKey(d => d.FkEstadoId)
                    .HasConstraintName("FK__tipo_docu__fk_es__46E78A0C");
            });

            modelBuilder.Entity<TipoHabitacion>(entity =>
            {
                entity.ToTable("tipo_habitacion");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(500)
                    .IsUnicode(false)
                    .HasColumnName("descripcion");

                entity.Property(e => e.FkEstadoId).HasColumnName("fk_estado_id");

                entity.HasOne(d => d.FkEstado)
                    .WithMany(p => p.TipoHabitacions)
                    .HasForeignKey(d => d.FkEstadoId)
                    .HasConstraintName("FK__tipo_habi__fk_es__4CA06362");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
