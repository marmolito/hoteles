﻿using System;
using System.Collections.Generic;

namespace Hoteles.Models.Entities
{
    public partial class Reserva
    {
        public int Id { get; set; }
        public int? FkPersonaId { get; set; }
        public int? FkHabitacion { get; set; }
        public DateTime? FechaInicio { get; set; }
        public DateTime? FechaFin { get; set; }
        public int? FkEstadoId { get; set; }

        public virtual Estado? FkEstado { get; set; }
        public virtual Habitacion? FkHabitacionNavigation { get; set; }
        public virtual Persona? FkPersona { get; set; }
    }
}
