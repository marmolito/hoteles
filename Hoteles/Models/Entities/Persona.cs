﻿using System;
using System.Collections.Generic;

namespace Hoteles.Models.Entities
{
    public partial class Persona
    {
        public Persona()
        {
            Reservas = new HashSet<Reserva>();
        }

        public int Id { get; set; }
        public string? Nombres { get; set; }
        public string? Apellidos { get; set; }
        public DateTime? FechaNacimiento { get; set; }
        public string? NumeroDocumento { get; set; }
        public int? FkExtensionId { get; set; }
        public string? Telefono { get; set; }
        public string? Correo { get; set; }
        public string? Contraseña { get; set; }
        public int? FkExtensionContactoId { get; set; }
        public string? TelefonoContacto { get; set; }
        public string? NombreContacto { get; set; }
        public int? FkGeneroId { get; set; }
        public int? FkDocumentoId { get; set; }
        public int? FkEstadoId { get; set; }

        public virtual TipoDocumento? FkDocumento { get; set; }
        public virtual Estado? FkEstado { get; set; }
        public virtual Extension? FkExtension { get; set; }
        public virtual Extension? FkExtensionContacto { get; set; }
        public virtual Genero? FkGenero { get; set; }
        public virtual ICollection<Reserva> Reservas { get; set; }
    }
}
