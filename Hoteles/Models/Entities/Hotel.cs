﻿using System;
using System.Collections.Generic;

namespace Hoteles.Models.Entities
{
    public partial class Hotel
    {
        public Hotel()
        {
            Habitacions = new HashSet<Habitacion>();
        }

        public int Id { get; set; }
        public string? Nit { get; set; }
        public string? Nombre { get; set; }
        public string? Direccion { get; set; }
        public int? FkCiudadId { get; set; }
        public int? FkDepartamentoId { get; set; }
        public int? FkPaisId { get; set; }
        public int? FkEstadoId { get; set; }

        public virtual Ciudad? FkCiudad { get; set; }
        public virtual Departamento? FkDepartamento { get; set; }
        public virtual Estado? FkEstado { get; set; }
        public virtual Pais? FkPais { get; set; }
        public virtual ICollection<Habitacion> Habitacions { get; set; }
    }
}
