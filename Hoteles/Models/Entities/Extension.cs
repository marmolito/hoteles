﻿using System;
using System.Collections.Generic;

namespace Hoteles.Models.Entities
{
    public partial class Extension
    {
        public Extension()
        {
            PersonaFkExtensionContactos = new HashSet<Persona>();
            PersonaFkExtensions = new HashSet<Persona>();
        }

        public int Id { get; set; }
        public string? Numero { get; set; }
        public int? FkEstadoId { get; set; }
        public int? FkPaisId { get; set; }

        public virtual Estado? FkEstado { get; set; }
        public virtual Pais? FkPais { get; set; }
        public virtual ICollection<Persona> PersonaFkExtensionContactos { get; set; }
        public virtual ICollection<Persona> PersonaFkExtensions { get; set; }
    }
}
