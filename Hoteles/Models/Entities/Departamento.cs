﻿using System;
using System.Collections.Generic;

namespace Hoteles.Models.Entities
{
    public partial class Departamento
    {
        public Departamento()
        {
            Ciudads = new HashSet<Ciudad>();
            Hotels = new HashSet<Hotel>();
        }

        public int Id { get; set; }
        public string? Nombre { get; set; }
        public int? FkPais { get; set; }
        public int? FkEstadoId { get; set; }

        public virtual Estado? FkEstado { get; set; }
        public virtual Pais? FkPaisNavigation { get; set; }
        public virtual ICollection<Ciudad> Ciudads { get; set; }
        public virtual ICollection<Hotel> Hotels { get; set; }
    }
}
