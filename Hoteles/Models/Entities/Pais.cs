﻿using System;
using System.Collections.Generic;

namespace Hoteles.Models.Entities
{
    public partial class Pais
    {
        public Pais()
        {
            Departamentos = new HashSet<Departamento>();
            Extensions = new HashSet<Extension>();
            Hotels = new HashSet<Hotel>();
        }

        public int Id { get; set; }
        public string? Nombre { get; set; }
        public int? FkEstadoId { get; set; }

        public virtual Estado? FkEstado { get; set; }
        public virtual ICollection<Departamento> Departamentos { get; set; }
        public virtual ICollection<Extension> Extensions { get; set; }
        public virtual ICollection<Hotel> Hotels { get; set; }
    }
}
