﻿using System;
using System.Collections.Generic;

namespace Hoteles.Models.Entities
{
    public partial class Estado
    {
        public Estado()
        {
            Camas = new HashSet<Cama>();
            Ciudads = new HashSet<Ciudad>();
            Departamentos = new HashSet<Departamento>();
            Extensions = new HashSet<Extension>();
            Generos = new HashSet<Genero>();
            Habitacions = new HashSet<Habitacion>();
            Hotels = new HashSet<Hotel>();
            Pais = new HashSet<Pais>();
            Personas = new HashSet<Persona>();
            Reservas = new HashSet<Reserva>();
            TipoDocumentos = new HashSet<TipoDocumento>();
            TipoHabitacions = new HashSet<TipoHabitacion>();
        }

        public int Id { get; set; }
        public string? Nombre { get; set; }
        public bool? Estado1 { get; set; }

        public virtual ICollection<Cama> Camas { get; set; }
        public virtual ICollection<Ciudad> Ciudads { get; set; }
        public virtual ICollection<Departamento> Departamentos { get; set; }
        public virtual ICollection<Extension> Extensions { get; set; }
        public virtual ICollection<Genero> Generos { get; set; }
        public virtual ICollection<Habitacion> Habitacions { get; set; }
        public virtual ICollection<Hotel> Hotels { get; set; }
        public virtual ICollection<Pais> Pais { get; set; }
        public virtual ICollection<Persona> Personas { get; set; }
        public virtual ICollection<Reserva> Reservas { get; set; }
        public virtual ICollection<TipoDocumento> TipoDocumentos { get; set; }
        public virtual ICollection<TipoHabitacion> TipoHabitacions { get; set; }
    }
}
