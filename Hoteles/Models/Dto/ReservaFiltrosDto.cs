﻿namespace Hoteles.Models.Dto
{
    public class ReservaFiltrosDto
    {
        public DateTime fecha_inicio { get; set; }
        public DateTime fecha_fin { get; set; }
        public int cantidad_alojamiento { get; set; }
        public string id_ciudad { get; set; }
        public string? habitacion { get; set; }
    }
}