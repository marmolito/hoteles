﻿namespace Hoteles.Models.Dto
{
    public class ReservaInfoDto
    {
        public int id { get; set; }
        public string numero { get; set; }
        public string tipo_habitacion { get; set; }
        public float costo_base { get; set; }
        public int cantidad_alojamiento { get; set; }
        public bool desayuno { get; set; }
        public string descripcion { get; set; }
        public string nombres { get; set; }
        public string apellidos { get; set; }
        public string documento { get; set; }
        public string numero_documento { get; set; }
        public string extension { get; set; }
        public string telefono { get; set; }
        public DateTime fecha_inicio { get; set; }
        public DateTime fecha_fin { get; set; }
    }
}