﻿namespace Hoteles.Models.Dto
{
    public class ReservaDisponibleDto
    {
        public int id { get; set; }
        public string hotel { get; set; }
        public string direccion { get; set; }
        public string ciudad { get; set; }
        public string numero_habitacion { get; set; }
        public float costo_base { get; set; }
        public int impuestos { get; set; }
        public string camas { get; set; }
        public int cantidad_alojamiento { get; set; }
        public bool desayuno { get; set; }
        public bool cancelacion_gratuita { get; set; }
        public string fecha_inicio { get; set; }
        public string fecha_fin { get; set; }
    }
}