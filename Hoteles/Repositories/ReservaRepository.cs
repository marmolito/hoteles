﻿using Dapper;
using Hoteles.Intefaces.RepositoryPattern;
using Hoteles.Models.Dto;
using Hoteles.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System.Data;
using System.Data.SqlClient;

namespace Hoteles.Services
{
    public class ReservaRepository : IReservaRepository
    {

        public readonly HotelesContext _hotelesContext;
        private readonly string _connectionString;

        public ReservaRepository(HotelesContext hotelesContext, IConfiguration configuration)
        {
            _hotelesContext = hotelesContext;
            _connectionString = configuration.GetConnectionString("cadenaSQL");
        }

        public async Task<List<ReservaInfoDto>> ObtenerReservas()
        {
            using var connection = new SqlConnection(_connectionString);

            try
            {
                var sql = $@"SELECT 
                                        r.id,
                                        h.numero, 
                                        th.descripcion AS tipo_habitacion,
                                        h.costo_base, 
                                        h.cantidad_alojamiento, 
                                        h.cancelacion_gratuita, 
                                        h.desayuno, 
                                        h.descripcion,
                                        p.nombres,
                                        p.apellidos,
                                        td.documento,
                                        p.numero_documento,
                                        e.numero AS extension,
                                        p.telefono,
                                        r.fecha_inicio,
                                        r.fecha_fin
                                    FROM reserva r
                                    INNER JOIN habitacion h ON h.id = r.fk_habitacion
                                    INNER JOIN persona p ON p.id = r.fk_persona_id
                                    INNER JOIN tipo_documento td ON td.id = p.fk_documento_id
                                    INNER JOIN tipo_habitacion th ON th.id = h.fk_tipo_id
                                    INNER JOIN extension e ON e.id = p.fk_extension_id
                                    ";

                var reservas = await connection.QueryAsync<ReservaInfoDto>(sql);

                return (List<ReservaInfoDto>)reservas;
            }
            catch (Exception ex)
            {
                throw new Exception($"Error al buscar reservas. {ex.Message}" );
            }
        }

        public async Task<List<ReservaDisponibleDto>> ObtenerOpcionesReservas(ReservaFiltrosDto filtros)
        {
            using var connection = new SqlConnection(_connectionString);

            try
            {
                var sql = $@"SELECT 
	                            ho.nombre AS  hotel,
	                            ho.direccion,
	                            h.id,
	                            c.nombre AS ciudad,
                                h.numero AS numero_habitacion, 
                                h.costo_base, 
	                            h.impuestos,
	                            ca.descripcion AS camas,
                                h.cantidad_alojamiento,
	                            h.desayuno,
	                            h.cancelacion_gratuita,
	                            r.fecha_inicio,
	                            r.fecha_fin
                            FROM 
                                habitacion h
	                            INNER JOIN hotel ho ON ho.id = h.fk_hotel_id
	                            INNER JOIN ciudad c ON c.id = ho.fk_ciudad_id
	                            INNER JOIN cama ca ON ca.id = h.fk_camas_id
	                            LEFT JOIN reserva r ON r.fk_habitacion = h.id 
	                            AND ((r.fecha_fin > @fecha_inicio AND r.fecha_fin <= @fecha_fin) OR (r.fecha_inicio >= @fecha_inicio AND r.fecha_inicio < @fecha_fin))
                            WHERE 
	                            (h.cantidad_alojamiento >= @cantidad) AND 
	                            (c.id = @ciudad) 
	                            AND r.id IS Null;";

                var paremeters = new DynamicParameters();
                paremeters.Add("@fecha_inicio", filtros.fecha_inicio, DbType.DateTime, ParameterDirection.Input);
                paremeters.Add("@fecha_fin", filtros.fecha_fin, DbType.DateTime, ParameterDirection.Input);
                paremeters.Add("@cantidad", filtros.cantidad_alojamiento, DbType.Int64, ParameterDirection.Input);
                paremeters.Add("@ciudad", filtros.id_ciudad, DbType.String, ParameterDirection.Input);

                var reservasDispoonibles = await connection.QueryAsync<ReservaDisponibleDto>(sql, paremeters);

                return (List<ReservaDisponibleDto>)reservasDispoonibles;
            }
            catch (Exception ex)
            {
                throw new Exception($"Error al buscar reservas disponibles. {ex.Message}");
            }
        }

    }
}
