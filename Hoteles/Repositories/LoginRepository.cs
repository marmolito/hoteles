﻿using Dapper;
using hoteles.Services;
using Hoteles.Intefaces.RepositoryPattern;
using Hoteles.Models.Dto;
using Hoteles.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;

namespace Hoteles.Services
{
    public class LoginRepository : ILoginRepository
    {

        public readonly HotelesContext _hotelesContext;
        private readonly string _connectionString;

        public LoginRepository(HotelesContext hotelesContext, IConfiguration configuration)
        {
            _hotelesContext = hotelesContext;
            _connectionString = configuration.GetConnectionString("cadenaSQL");
        }

        public string obtenerTokenLogin(string email, string password)
        {
            Encriptacion encrip = new Encriptacion();
            string fecha = DateTime.UtcNow.ToString("yyyyMMddHHmmss");
            string tokenLogin = encrip.AES256_Encriptar(encrip.AES256_LOGIN_Key, fecha + '#' + email + '#' + encrip.GetSHA256(password));
            return tokenLogin;
        }
         public string LoginByToken(string loginToken)
        {
            try
            {
                Encriptacion encrip = new Encriptacion();
                string tokenUsuario = "";

                string tokenDescoficado = encrip.AES256_Desencriptar(encrip.AES256_LOGIN_Key, loginToken);
                string fecha = tokenDescoficado.Split('#')[0];
                string email = tokenDescoficado.Split('#')[1];
                string password = tokenDescoficado.Split('#')[2];

                // Validar fecha
                DateTime fechaLogin = DateTime.ParseExact(fecha, "yyyyMMddHHmmss", System.Globalization.CultureInfo.InvariantCulture);
                if (DateTime.UtcNow.Subtract(fechaLogin).TotalSeconds >= 30)
                {
                    return "-1";// -1 = Límite de tiempo excedido
                }

                // Validar login

                List< Persona> persona = _hotelesContext.Personas.Where(h => h.Correo == email && h.Contraseña == password).ToList();
                Persona usuario = persona.FirstOrDefault();

                if (persona.Count > 0)
                {
                    tokenUsuario = usuario.Correo.ToString() + "#" + DateTime.UtcNow.AddHours(18).ToString("yyyyMMddHHmmss");        // Email # FechaCaducidad -> Encriptar con AES
                    tokenUsuario = encrip.AES256_Encriptar(encrip.AES256_USER_Key, tokenUsuario);
                    return tokenUsuario;
                }
                else
                {
                    return "-2"; // -2 = Usuario o clave incorrectas
                }
            }
            catch (Exception)
            {
                return "-3"; // -3 = Error
            }
        }

        public bool ValidarTokenUsuario(string tokenUsuario)
        {
            try
            {
                Encriptacion encrip = new Encriptacion();
                tokenUsuario = encrip.CorregirToken(tokenUsuario);
                string tokenDescodificado = encrip.AES256_Desencriptar(encrip.AES256_USER_Key, tokenUsuario);
                string emailUsuario = tokenDescodificado.Split('#')[0];
                string fecha = tokenDescodificado.Split('#')[1];

                // Validar fecha
                DateTime fechaCaducidad = DateTime.ParseExact(fecha, "yyyyMMddHHmmss", System.Globalization.CultureInfo.InvariantCulture);
                if (DateTime.UtcNow > fechaCaducidad)
                    return false;
                else
                    return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

    }
}
