﻿using Hoteles.Intefaces.RepositoryPattern;
using Hoteles.Models.Dto;
using Hoteles.Models.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Microsoft.AspNetCore.Cors;
using MailKit;
using Hoteles.Models;
using Hoteles.Services;

namespace Hoteles.Controllers
{
    [EnableCors("ReglasCors")]
    [Route("reservas")]
    [ApiController]
    public class ReservaController : ControllerBase
    {

        public readonly HotelesContext _hotelesContext;
        public readonly IReservaRepository _reservaRepository;
        private readonly IMailService _mailService;
        public readonly ILoginRepository _loginRepository;
        public ReservaController(HotelesContext hotelesContext, IReservaRepository reservaRepository, IMailService _MailService, ILoginRepository loginRepository)
        {
            _hotelesContext = hotelesContext;
            _reservaRepository = reservaRepository;
            _mailService = _MailService;
            _loginRepository = loginRepository;
        }

        [HttpGet]
        [Route("listar")]
        public async Task<IActionResult> Obtener()
        {
            if (Request.Headers.TryGetValue("token", out var tokenValidacion))
            {
                if (!_loginRepository.ValidarTokenUsuario(tokenValidacion))
                    return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "Token caducado o incorrecto. " });
            }
            else
                return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "Se requiere Token de Autenticación. " });

            try
            {
                var reservas = await _reservaRepository.ObtenerReservas();

                if (reservas == null)
                {
                    return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "No se encontraron reservas." });
                }

                return StatusCode(StatusCodes.Status200OK, new { mensaje = "Reservas. ", Response = reservas });
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "Error al buscar reservas. " + ex.Message });
            }
        }

        [HttpGet]
        [Route("listar/{idReserva:int}")]
        public async Task<IActionResult> ObtenerPorId(int idReserva)
        {
            if (Request.Headers.TryGetValue("token", out var tokenValidacion))
            {
                if (!_loginRepository.ValidarTokenUsuario(tokenValidacion))
                    return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "Token caducado o incorrecto. " });
            }
            else
                return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "Se requiere Token de Autenticación. " });

            try
            {
                var reservas = await _reservaRepository.ObtenerReservas();

                if (reservas == null)
                {
                    return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "No se encontraron reservas." });
                }

                var reservaBuscada = reservas.FirstOrDefault(reserva => reserva.id == idReserva);

                return StatusCode(StatusCodes.Status200OK, new { mensaje = "Reservas. ", Response = reservaBuscada });
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "Error al buscar reservas. " + ex.Message });
            }
        }

        [HttpPost]
        [Route("listar/habitacion-disponible")]
        public async Task<IActionResult> ObtenerHabitacionDisponible([FromBody] ReservaFiltrosDto objeto)
        {
            if (Request.Headers.TryGetValue("token", out var tokenValidacion))
            {
                if (!_loginRepository.ValidarTokenUsuario(tokenValidacion))
                    return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "Token caducado o incorrecto. " });
            }
            else
                return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "Se requiere Token de Autenticación. " });

            try
            {
                var reservasDisponibles = await _reservaRepository.ObtenerOpcionesReservas(objeto);

                if (reservasDisponibles == null)
                {
                    return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "No se encontraron opciones de reserva." });
                }

                return StatusCode(StatusCodes.Status200OK, new { mensaje = "Opciones de reserva. ", Response = reservasDisponibles });
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "Error al buscar opciones de reserva. " + ex.Message });
            }


        }

        [HttpPost]
        [Route("reservar")]
        public async Task<IActionResult> Reservar([FromBody] Reserva objeto)
        {
            if (Request.Headers.TryGetValue("token", out var tokenValidacion))
            {
                if (!_loginRepository.ValidarTokenUsuario(tokenValidacion))
                    return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "Token caducado o incorrecto. " });
            }
            else
                return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "Se requiere Token de Autenticación. " });

            try
            {
                //realizar reserva
                objeto.FkEstadoId = 3;

                _hotelesContext.Reservas.Add(objeto);
                var reserva = _hotelesContext.SaveChanges();

                //obtener informacion de persona

                Persona persona = _hotelesContext.Personas.Find(objeto.FkPersonaId);

                //enviar correo de confirmacion

                MailData mailData = new MailData()
                {
                    EmailToName = persona.Nombres + " " + persona.Apellidos,
                    EmailToId = persona.Correo,
                    EmailSubject = "Confirmacion de Reserva",
                    EmailBody = "Su reservacion ha sido registrada con exito. Numero de reserva: ",
                };

                bool correo = _mailService.SendMail(mailData);

                if (correo)
                {
                    return StatusCode(StatusCodes.Status200OK, new { mensaje = "Reserva creada correctamente." });
                }
                else
                {
                    return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "Reserva creada correctamente. ERROR al enviar el correo" });
                }

            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "Erro al crear Reserva. " + ex.Message });
            }
        }


    }


}

