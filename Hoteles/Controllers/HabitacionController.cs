﻿using Hoteles.Models.Dto;
using Hoteles.Models.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Cors;
using Hoteles.Intefaces.RepositoryPattern;

namespace Hoteles.Controllers
{
    [EnableCors("ReglasCors")]
    [Route("habitaciones")]
    [ApiController]
    public class HabitacionController : ControllerBase
    {

        public readonly HotelesContext _hotelesContext;
        public readonly ILoginRepository _loginRepository;
        public HabitacionController(HotelesContext hotelesContext, ILoginRepository loginRepository)
        {
            _hotelesContext = hotelesContext;
            _loginRepository = loginRepository;
        }

        [HttpPost]
        [Route("crear")]
        public IActionResult Crear([FromBody] Habitacion objeto)
        {
            if (Request.Headers.TryGetValue("token", out var tokenValidacion))
            {
                if (!_loginRepository.ValidarTokenUsuario(tokenValidacion))
                    return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "Token caducado o incorrecto. " });
            }
            else
                return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "Se requiere Token de Autenticación. " });

            try
            {
                _hotelesContext.Habitacions.Add(objeto);
                _hotelesContext.SaveChanges();

                return StatusCode(StatusCodes.Status200OK, new { mensaje = "Habitación creada correctamente." });
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "Erro al crear habitación. " + ex.Message });
            }
        }

        [HttpPut]
        [Route("editar")]
        public IActionResult Editar([FromBody] Habitacion objeto)
        {
            if (Request.Headers.TryGetValue("token", out var tokenValidacion))
            {
                if (!_loginRepository.ValidarTokenUsuario(tokenValidacion))
                    return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "Token caducado o incorrecto. " });
            }
            else
                return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "Se requiere Token de Autenticación. " });

            try
            {
                Habitacion habitacion = _hotelesContext.Habitacions.Find(objeto.Id);

                if (habitacion == null)
                {
                    return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "Habitación no encontrada." });

                }


                habitacion.Numero = objeto.Numero is null ? habitacion.Numero : objeto.Numero;
                habitacion.CostoBase = objeto.CostoBase is null ? habitacion.CostoBase : objeto.CostoBase;
                habitacion.Impuestos = objeto.Impuestos is null ? habitacion.Impuestos : objeto.Impuestos;
                habitacion.CantidadAlojamiento = objeto.CantidadAlojamiento is null ? habitacion.CantidadAlojamiento : objeto.CantidadAlojamiento;
                habitacion.Descripcion = objeto.Descripcion is null ? habitacion.Descripcion : objeto.Descripcion;
                habitacion.Desayuno = objeto.Desayuno is null ? habitacion.Desayuno : objeto.Desayuno;
                habitacion.CancelacionGratuita = objeto.CancelacionGratuita is null ? habitacion.CancelacionGratuita : objeto.CancelacionGratuita;
                habitacion.FkTipoId = objeto.FkTipoId is null ? habitacion.FkTipoId : objeto.FkTipoId;
                habitacion.FkCamasId = objeto.FkCamasId is null ? habitacion.FkCamasId : objeto.FkCamasId;
                habitacion.FkHotelId = objeto.FkHotelId is null ? habitacion.FkHotelId : objeto.FkHotelId;
                habitacion.FkEstadoId = objeto.FkEstadoId is null ? habitacion.FkEstadoId : objeto.FkEstadoId;

                _hotelesContext.Habitacions.Update(habitacion);
                _hotelesContext.SaveChanges();

                return StatusCode(StatusCodes.Status200OK, new { mensaje = "Habitación actualizada correctamente." });
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "Error al actualizar Habitación. " + ex.Message });
            }

        }

        [HttpPut]
        [Route("habilitar")]
        public IActionResult Habilitar([FromBody] Habitacion objeto)
        {
            if (Request.Headers.TryGetValue("token", out var tokenValidacion))
            {
                if (!_loginRepository.ValidarTokenUsuario(tokenValidacion))
                    return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "Token caducado o incorrecto. " });
            }
            else
                return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "Se requiere Token de Autenticación. " });

            try
            {
                Habitacion habitacion = _hotelesContext.Habitacions.Find(objeto.Id);

                if (habitacion == null)
                {
                    return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "Habitación no encontrada." });
                }

                habitacion.FkEstadoId = objeto.FkEstadoId is null ? habitacion.FkEstadoId : objeto.FkEstadoId;

                _hotelesContext.Habitacions.Update(habitacion);
                _hotelesContext.SaveChanges();

                return StatusCode(StatusCodes.Status200OK, new { mensaje = "Status de habitación actualizado" });
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "Error al actualizar habitación. " + ex.Message });
            }
        }

        [HttpGet]
        [Route("obtener/habitacion-hotel/{idHotel:Int}")]
        public IActionResult ObtenerHabitacionPorHotel(int idHotel)
        {
            if (Request.Headers.TryGetValue("token", out var tokenValidacion))
            {
                if (!_loginRepository.ValidarTokenUsuario(tokenValidacion))
                    return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "Token caducado o incorrecto. " });
            }
            else
                return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "Se requiere Token de Autenticación. " });


            List<Habitacion> habitacion = _hotelesContext.Habitacions.Where(h => h.FkHotelId == idHotel).ToList();

            if (habitacion.Count == 0)
            {
                return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "Habitaciones de Hotel no encontradas." });
            }
            try
            {
                return StatusCode(StatusCodes.Status200OK, new { mensaje = "Habitaciones de Hotel", response = habitacion });

            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "Error al encontrar Habitaciones de Hotel. ", response = habitacion });


            }
        }


    }


}

