﻿using Hoteles.Intefaces.RepositoryPattern;
using Hoteles.Models.Entities;
using Hoteles.Services;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Hoteles.Controllers
{
    [EnableCors("ReglasCors")]
    [Route("personas")]
    [ApiController]
    public class PersonaController : ControllerBase
    {

        private readonly HotelesContext _hotelesContext;
        private readonly ILoginRepository _loginRepository;
        public PersonaController(HotelesContext hotelesContext, ILoginRepository loginRepository)
        {
            _hotelesContext = hotelesContext;
            _loginRepository = loginRepository;
        }

        [HttpGet]
        [Route("listar")]
        public IActionResult Obtener()
        {
            if (Request.Headers.TryGetValue("token", out var tokenValidacion))
            {
                if (!_loginRepository.ValidarTokenUsuario(tokenValidacion))
                    return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "Token caducado o incorrecto. " });
            }
            else
                return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "Se requiere Token de Autenticación. " });

            try
            {
                List<Persona> personas = new List<Persona>();

                personas =  _hotelesContext.Personas.Include(p => p.Reservas).ToList();

                if (personas.Count < 0)
                {
                    return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "No se encontraron personas." });
                }

                return StatusCode(StatusCodes.Status200OK, new { mensaje = "Personas. ", Response = personas });
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "Error al buscar personas. " + ex.Message });
            }
        }


    }



}
