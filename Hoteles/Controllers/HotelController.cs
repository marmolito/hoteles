﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Hoteles.Models.Entities;
using Microsoft.AspNetCore.Cors;
using Hoteles.Intefaces.RepositoryPattern;

namespace Hoteles.Controllers
{
    [EnableCors("ReglasCors")]
    [Route("hoteles")]
    [ApiController]
    public class HotelController : Controller
    {

        public readonly HotelesContext _hotelesContext;
        public readonly ILoginRepository _loginRepository;

        public HotelController(HotelesContext hotelesContext, ILoginRepository loginRepository)
        {
            _hotelesContext = hotelesContext;
            _loginRepository = loginRepository;
        }

        [HttpGet]
        [Route("listar")]
        public IActionResult Buscar()
        {
            if (Request.Headers.TryGetValue("token", out var tokenValidacion))
            {
                if (!_loginRepository.ValidarTokenUsuario(tokenValidacion))
                    return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "Token caducado o incorrecto. " });
            }
            else
                return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "Se requiere Token de Autenticación. " });

            try
            {
                List<Hotel> hoteles = new List<Hotel>();

                hoteles = _hotelesContext.Hotels.ToList();
                return StatusCode(StatusCodes.Status200OK, new { mensaje = "Lista de Hoteles.", Response = hoteles });
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "Error al buscar Hoteles. " + ex.Message });

            }
        }

        [HttpPost]
        [Route("crear")]
        public IActionResult Crear([FromBody] Hotel objeto)
        {
            if (Request.Headers.TryGetValue("token", out var tokenValidacion))
            {
                if (!_loginRepository.ValidarTokenUsuario(tokenValidacion))
                    return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "Token caducado o incorrecto. " });
            }
            else
                return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "Se requiere Token de Autenticación. " });

            try
            {
                _hotelesContext.Hotels.Add(objeto);
                _hotelesContext.SaveChanges();

                return StatusCode(StatusCodes.Status200OK, new { mensaje = "Hotel creado correctamente." });
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "Error al crear Hotel. " + ex.Message });
            }
        }

        [HttpGet]
        [Route("obtener/{idHotel:int}")]
        public IActionResult ObtenerPor(int idHotel)
        {
            if (Request.Headers.TryGetValue("token", out var tokenValidacion))
            {
                if (!_loginRepository.ValidarTokenUsuario(tokenValidacion))
                    return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "Token caducado o incorrecto. " });
            }
            else
                return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "Se requiere Token de Autenticación. " });

            Hotel hotel = _hotelesContext.Hotels.Find(idHotel);

            if (hotel == null)
            {
                return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "Hotel no encontrado." });
            }

            try
            {
                hotel = _hotelesContext.Hotels.Include(c => c.Habitacions).Where(p => p.Id == idHotel).FirstOrDefault();

                return StatusCode(StatusCodes.Status200OK, new { mensaje = "Hotel. ", response = hotel });

            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "Error al encontrar el hotel. ", response = hotel });


            }
        }

        [HttpPut]
        [Route("editar")]
        public IActionResult Editar([FromBody] Hotel objeto)
        {
            Hotel hotel = _hotelesContext.Hotels.Find(objeto.Id);

            if (hotel == null)
            {

                return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "Hotel no encontrado." });

            }

            try
            {
                hotel.Nit = objeto.Nit is null ? hotel.Nit : objeto.Nit;
                hotel.Nombre = objeto.Nombre is null ? hotel.Nombre : objeto.Nombre;
                hotel.Direccion = objeto.Direccion is null ? hotel.Direccion : objeto.Direccion;
                hotel.FkCiudadId = objeto.FkCiudadId is null ? hotel.FkCiudadId : objeto.FkCiudadId;
                hotel.FkDepartamentoId = objeto.FkDepartamentoId is null ? hotel.FkDepartamentoId : objeto.FkDepartamentoId;
                hotel.FkPaisId = objeto.FkPaisId is null ? hotel.FkPaisId : objeto.FkPaisId;
                hotel.FkEstadoId = objeto.FkEstadoId is null ? hotel.FkEstadoId : objeto.FkEstadoId;

                _hotelesContext.Hotels.Update(hotel);
                _hotelesContext.SaveChanges();

                return StatusCode(StatusCodes.Status200OK, new { mensaje = "Hotel actualizado con Exito." });
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "Error al actualizar el hotel, verifique los parametros." + ex.Message });
            }

        }

        [HttpPut]
        [Route("habilitar")]
        public IActionResult Habilitar([FromBody] Hotel objeto)
        {
            Hotel hotel = _hotelesContext.Hotels.Find(objeto.Id);

            if (hotel == null)
            {
                return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "Hotel no encontrado." });
            }

            try
            {
                hotel.FkEstadoId = objeto.FkEstadoId is null ? hotel.FkEstadoId : objeto.FkEstadoId;

                _hotelesContext.Hotels.Update(hotel);
                _hotelesContext.SaveChanges();

                return StatusCode(StatusCodes.Status200OK, new { mensaje = "Status de Hotel actualizado" });
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "Error al actualizar Hotel. " + ex.Message });
            }
        }

    }



}


