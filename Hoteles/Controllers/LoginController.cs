﻿using Hoteles.Intefaces.RepositoryPattern;
using Hoteles.Models;
using Hoteles.Models.Dto;
using Hoteles.Services;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Hoteles.Controllers
{
    [EnableCors("ReglasCors")]
    [Route("login")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        public readonly ILoginRepository _loginRepository;

        public LoginController(ILoginRepository loginRepository)
        {
            _loginRepository = loginRepository;
        }

        [HttpPost]
        [Route("login")]
        public IActionResult LoginPorToken([FromBody] loginDto usuario)
        {
            string tokenLogin = _loginRepository.obtenerTokenLogin(usuario.correo, usuario.contraseña);
            string token = _loginRepository.LoginByToken(tokenLogin);

            switch (token)
            {
                case "-1": return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "Límite de tiempo excedido. " });
                case "-2": return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "Usuario o clave incorrectos. " });
                case "-3": return StatusCode(StatusCodes.Status400BadRequest, new { mensaje = "No se pudo hacer el login, revise los datos enviados. " });
                default: return StatusCode(StatusCodes.Status200OK, new { mensaje = token });
            }
        }

        [HttpPost]
        [Route("environmet")]
        public IActionResult setEnvironment([FromBody] EnvironmentValue dato)
        {

            if (dato.environment != "1" && dato.environment != "2")
                dato.environment = "3";

            switch (dato.environment)
            {
                case "1": return StatusCode(StatusCodes.Status200OK, new { environmet = "http://localhost:5107" });
                case "2": return StatusCode(StatusCodes.Status200OK, new { environmet = "http://www.api-hotes.somee.com" });
            }

            return StatusCode(StatusCodes.Status400BadRequest, new { environmet = "Digite 1 para entorno local, 2 para entorno Dev (web)." });
        }

    }


}

