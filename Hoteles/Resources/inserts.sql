--inserts de tablas con informacion masiva

INSERT INTO estados VALUES
('Activado', 1),
('Desactivado', 1),
('Reservado', 1),
('Cancelado', 1),
('Modificado', 1);

INSERT INTO pais VALUES
('Colombia', '1'),
('Espa�a', '2');

INSERT INTO departamento VALUES
('Cundinamarca', '1', '1'),
('Antioquia', '1', '1');

INSERT INTO ciudad VALUES
('Zipaquir�', '1', '1'),
('Guatavita', '1', '1'),
('Bogot�', '1', '1'),
('Suesca', '1', '1'),
('Nemoc�n', '1', '1'),
('Medell�n', '2', '1'),
('Puerto Berr�o', '2', '1'),
('Marinilla', '2', '1'),
('Concepci�n', '2', '1'),
('Donmat�as', '2', '1');

INSERT INTO genero VALUES
('Masculino', '1'),
('Femenino', '1'),
('Otro', '1');

INSERT INTO tipo_documento VALUES
('C�dula Ciudadan�a', '1'),
('C�dula Extranjer�a', '1'),
('Pasaporte', '1');

INSERT INTO cama VALUES
('1 cama doble', '1'),
('2 camas dobles', '1'),
('1 cama sencilla', '1'),
('2 cama sencillas', '1'),
('1 cama doble, 1 cama sencilla', '1');

INSERT INTO tipo_habitacion VALUES
('Suite', '1'),
('Junior suite', '1'),
('Gran suite', '1'),
('Habitaciones matrimoniales', '1'),
('Habitaciones familiares', '1');

INSERT INTO extension VALUES
('57', '1', '1'),
('34', '1', '2');

INSERT INTO hotel Values
('123456710', 'Orquidea', 'Calle 53 a 12 - 22', '1', '1', '1', '1');

INSERT INTO habitacion VALUES
('101', '70000', '12', '2', '', 1, 1, '1', '1', '1', '1'),
('102', '70000', '12', '2', '', 1, 1, '1', '1', '1', '1'),
('103', '70000', '12', '2', '', 1, 1, '1', '1', '1', '1'),
('104', '70000', '12', '2', '', 1, 1, '1', '1', '1', '1'),
('201', '70000', '12', '2', '', 1, 1, '1', '1', '1', '1'),
('202', '70000', '12', '2', '', 1, 1, '1', '1', '1', '1'),
('203', '70000', '12', '2', '', 1, 1, '1', '1', '1', '1'),
('204', '70000', '12', '2', '', 1, 1, '1', '1', '1', '1');

INSERT INTO persona VALUES
('elkin fabian', 'marmolejo rodriguez', '1999-09-26', '1069768464', '1', '3214199479', 'fabianmarmolejo07@gmail.com', '3164EB9780FDCFEB545F25972F64E2A502B98A83476923D8A16BC186F4CAD8B9', '2', '3214556677', 'daniel ortiz', '1', '1', '1');

INSERT INTO reserva VALUES
('1', '2', '2023-12-20', '2023-12-21', '3'),
('1', '2', '2023-12-22', '2023-12-24', '3'),
('1', '2', '2023-12-11', '2023-12-13', '3'),
('1', '2', '2023-12-10', '2023-12-16', '3');

select * from estados;
select * from pais;
select * from departamento;
select * from ciudad;
select * from genero;
select * from tipo_documento;
select * from cama;
select * from tipo_habitacion
select * from extension;
select * from hotel;
select * from habitacion;
select * from persona;
select * from reserva;




