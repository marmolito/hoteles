--creacion de base de datos

CREATE DATABASE Hoteles

USE Hoteles

--tablas de informacion masiva

CREATE TABLE estados(
	id INT IDENTITY(1,1) PRIMARY KEY,
	nombre VARCHAR(500), 
	estado BIT
);

CREATE TABLE pais(
	id INT IDENTITY(1,1) PRIMARY KEY,
	nombre VARCHAR(500),
	fk_estado_id INT

	FOREIGN KEY (fk_estado_id) REFERENCES estados(id)
);

CREATE TABLE departamento(
	id INT IDENTITY(1,1) PRIMARY KEY,
	nombre VARCHAR(500),
	fk_pais INT,
	fk_estado_id INT

	FOREIGN KEY (fk_estado_id) REFERENCES estados(id),
	FOREIGN KEY (fk_pais) REFERENCES pais(id),
);

CREATE TABLE ciudad(
	id INT IDENTITY(1,1) PRIMARY KEY,
	nombre VARCHAR(500),
	fk_departamento INT,
	fk_estado_id INT

	FOREIGN KEY (fk_estado_id) REFERENCES estados(id),
	FOREIGN KEY (fk_departamento) REFERENCES departamento(id)
);

CREATE TABLE genero(
	id INT IDENTITY(1,1) PRIMARY KEY,
	descripcion VARCHAR(500),
	fk_estado_id INT

	FOREIGN KEY (fk_estado_id) REFERENCES estados(id)
);


CREATE TABLE tipo_documento(
	id INT IDENTITY(1,1) PRIMARY KEY,
	documento VARCHAR(500),
	fk_estado_id INT

	FOREIGN KEY (fk_estado_id) REFERENCES estados(id)
);

CREATE TABLE cama(
	id INT IDENTITY(1,1) PRIMARY KEY,
	descripcion VARCHAR(500),
	fk_estado_id INT

	FOREIGN KEY (fk_estado_id) REFERENCES estados(id)
);

CREATE TABLE tipo_habitacion(
	id INT IDENTITY(1,1) PRIMARY KEY,
	descripcion VARCHAR(500),
	fk_estado_id INT

	FOREIGN KEY (fk_estado_id) REFERENCES estados(id)
);

CREATE TABLE extension(
	id INT IDENTITY(1,1) PRIMARY KEY,
	numero VARCHAR(3),
	fk_estado_id INT,
	fk_pais_id INT

	FOREIGN KEY (fk_estado_id) REFERENCES estados(id),
	FOREIGN KEY (fk_pais_id) REFERENCES pais(id)
);
--tablas de entidades

CREATE TABLE hotel(
	id INT IDENTITY(1,1) PRIMARY KEY,
	nit VARCHAR(10),
	nombre VARCHAR(500),
	direccion VARCHAR(500),
	fk_ciudad_id INT,
	fk_departamento_id INT,
	fk_pais_id INT,
	fk_estado_id INT

	FOREIGN KEY (fk_estado_id) REFERENCES estados(id),
	FOREIGN KEY (fk_ciudad_id) REFERENCES ciudad(id),
	FOREIGN KEY (fk_departamento_id) REFERENCES departamento(id),
	FOREIGN KEY (fk_pais_id) REFERENCES pais(id),
);

CREATE TABLE habitacion(
	id INT IDENTITY(1,1) PRIMARY KEY,
	numero VARCHAR(4),
	costo_base FLOAT,
	impuestos INT,
	cantidad_alojamiento INT,
	descripcion VARCHAR(1000),
	desayuno BIT,
	cancelacion_gratuita BIT,
	fk_tipo_id INT,
	fk_camas_id INT,
	fk_hotel_id INT,
	fk_estado_id INT

	FOREIGN KEY (fk_estado_id) REFERENCES estados(id),
	FOREIGN KEY (fk_hotel_id) REFERENCES hotel(id),
	FOREIGN KEY (fk_camas_id) REFERENCES cama(id),
	FOREIGN KEY (fk_tipo_id) REFERENCES tipo_habitacion(id)
);

CREATE TABLE persona(
	id INT IDENTITY(1,1) PRIMARY KEY,
	nombres VARCHAR(100),
	apellidos VARCHAR(100),
	fecha_nacimiento DATE,
	numero_documento VARCHAR(10),
	fk_extension_id INT,
	telefono VARCHAR(10),
	correo VARCHAR(100),
	contraseņa VARCHAR(100),
	fk_extension_contacto_id INT,
	telefono_contacto VARCHAR(100),
	nombre_contacto VARCHAR(100),
	fk_genero_id INT,
	fk_documento_id INT,
	fk_estado_id INT

	FOREIGN KEY (fk_estado_id) REFERENCES estados(id),
	FOREIGN KEY (fk_genero_id) REFERENCES genero(id),
	FOREIGN KEY (fk_documento_id) REFERENCES tipo_documento(id),
	FOREIGN KEY (fk_extension_id) REFERENCES extension(id),
	FOREIGN KEY (fk_extension_contacto_id) REFERENCES extension(id)
);

CREATE TABLE reserva(
	id INT IDENTITY(1,1) PRIMARY KEY,
	fk_persona_id INT,
	fk_habitacion INT,
	fecha_inicio DATE,
	fecha_fin DATE,
	fk_estado_id INT

	FOREIGN KEY (fk_estado_id) REFERENCES estados(id),
	FOREIGN KEY (fk_persona_id) REFERENCES persona(id),
	FOREIGN KEY (fk_habitacion) REFERENCES habitacion(id)
);

